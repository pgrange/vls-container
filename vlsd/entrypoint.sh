#!/bin/sh
set -e

cp /vlsd2.toml $VLS_DATA/
sed -i "1s/^/network = \"$VLS_NETWORK\"\n/" $VLS_DATA/vlsd2.toml

if [ $(echo "$1" | cut -c1) = "-" ]; then
  echo "$0: assuming arguments for vlsd2"

  set -- vlsd2 "$@"
fi

if [ $(echo "$1" | cut -c1) = "-" ] || [ "$1" = "vlsd2" ]; then
  echo "$0: setting config to $VLS_DATA/vlsd2.toml"

  set -- "$@" --config=$VLS_DATA/vlsd2.toml
fi

echo
exec "$@"
