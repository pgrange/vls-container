FROM --platform=${TARGETPLATFORM:-${BUILDPLATFORM:-linux/amd64}} alpine:3.18 as builder

WORKDIR /build

RUN apk update && \
    apk add \
    build-base \
    cargo \
    git \
    grpc \
    protobuf \
    protobuf-dev \
    rust

ARG VLS_GIT_HASH \
    VLS_REPO
RUN echo "building vlsd from hash: $VLS_GIT_HASH"
RUN git clone $VLS_REPO vls
RUN cd vls && \
    git checkout $VLS_GIT_HASH && \
    cargo install --locked --path ./vls-proxy --bin vlsd2 --profile release --root /usr/local/ && \
    cargo install --locked --path ./vls-cli --profile release --root /usr/local/ && \
    cargo clean

FROM --platform=${TARGETPLATFORM:-${BUILDPLATFORM:-linux/amd64}} alpine:3.18 as runner

ARG UID=100
ARG GID=101

LABEL maintainer.0="Lakshya Singh (@king-11)" \
  maintainer.1="Dev Random (@devrandom01)"

RUN addgroup vls --gid ${GID} --system
RUN adduser --uid ${UID} --system vls --ingroup vls

RUN apk update && \
  apk add \
  build-base \
  curl-dev \
  protobuf \
  bind-tools

COPY --from=builder /usr/local/bin/vlsd2 /usr/local/bin/vlsd2
COPY --from=builder /usr/local/bin/vls-cli /usr/local/bin/vls-cli

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY healthcheck.sh /healthcheck.sh
RUN chmod +x /healthcheck.sh

COPY vlsd2.toml /vlsd2.toml

ENV VLS_DATA=/home/vls/.lightning-signer
RUN mkdir ${VLS_DATA}
RUN chown vls:vls ${VLS_DATA}

ENV REMOTE_SIGNER_ALLOWLIST=${VLS_DATA}/ALLOWLIST
RUN touch ${REMOTE_SIGNER_ALLOWLIST}

VOLUME ["${VLS_DATA}"]

USER vls

HEALTHCHECK --interval=10s --timeout=10s --start-period=10s \
    CMD ["/healthcheck.sh"]

ENTRYPOINT ["/entrypoint.sh"]

RUN vlsd2 --git-desc

CMD ["vlsd2"]
